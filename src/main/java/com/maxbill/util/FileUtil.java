package com.maxbill.util;

import lombok.extern.log4j.Log4j2;

import java.io.*;

@Log4j2
public class FileUtil {

    public static String readFileData(String filePath) {
        // 读取文件数据
        StringBuilder strbuffer = new StringBuilder();
        File file = new File(FileUtil.class.getResource(filePath).getFile());
        if (!file.exists()) {
            log.error("Can't Find File : {}", filePath);
        }
        try {
            FileInputStream fis = new FileInputStream(file);
            InputStreamReader inputStreamReader = new InputStreamReader(fis, "UTF-8");
            BufferedReader in = new BufferedReader(inputStreamReader);
            String str;
            while ((str = in.readLine()) != null) {
                strbuffer.append(str);
            }
            in.close();
        } catch (IOException e) {
            log.error("Read File Exception : {}", e.getMessage());
        }
        return strbuffer.toString();
    }

}
